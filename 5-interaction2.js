function switchLight() {
    
    var myImage = document.getElementById('myImage');
    var btSwitch = document.getElementById('btSwitch'); 
    
    //bascule d'allumé à éteint
    if (btSwitch.innerHTML == 'Allumer') {

        myImage.src = 'images/pic_bulbon.gif';
        btSwitch.innerHTML = 'Eteindre';

    } else {

        myImage.src = 'images/pic_bulboff.gif';
        btSwitch.innerHTML = 'Allumer';
        
    }

}